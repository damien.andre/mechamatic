# Mechamatic an webapp tool for mechanical characterization
You can see [here](https://www.unilim.fr/pages_perso/damien.andre/mechamatic/html/index.html) on 
online demo of mechamatic. 

## One minute running guide for linux (local host only)
To run mechamatic on a Linux computer this is easy, simply type the following commands
```
git clone git@gitlab.com:damien.andre/mechamatic.git
cd mechamatic
python ./python/run-server.py &
cd html 
sudo python -m http.server 80 --bind 127.0.0.1 &
```
And now, open your web browser and go to [http://0.0.0.0/](http://0.0.0.0/)
*That's all !*


## Run your own Mechamatic server
If you want to install mechamatic on your own server, you must run the 
python server located in the `python` directory. The following commands will run them : 
```
cd python
python ./run-server.py
```
Note that python servers will create some files in the current running directory. It should 
be okay if you use Mechamatic as local webserver. Due to security reason, if you want to run 
your own instance of Mechamatic online, you should run servers in a secure mode. To do that,
you must run the servers with :
```
cd python
$ python ./run-server.py --cert path_to_your_ssl_cert.pem --key  path_to_your_ssl_key.pem
```
You can change the port with `--port` command line option. By default, the 
9090 port is used for the acoustic app and the 9091 port for the optic port.


## Deploy your own Mechamatic client
Put somewhere the whole content of the `html` directory with the right file access.
Now, edit the `html/js/common.js` and put the address of your webserver as follow :
```
var server = "ws://your_address"
var aco_port = "9090/ws"
var opt_port = "9091/ws"
```
If you want to use a secure server please use the secure web socket protocol `wss://your_address` instead 
of the standard one `ws://your_adress`. 

In addition, you must set the variable `force_https` to `true` in the  `html/js/common.js` to ensure a 
secure usage. This is required because most of web browser does not allow usage of micro or camera in 
non secure modes (http).




