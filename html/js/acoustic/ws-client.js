// Copyright 2017 Damien Andre. This file is part of Mechamatic.

// Mechamatic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// () any later version.
  
// Mechamatic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
  
// You should have received a copy of the GNU General Public License
// along with Mechamatic.  If not, see <http://www.gnu.org/licenses/>.



var ws = new WebSocket(server + ":" + aco_port);
var result;

function sendmessage(msg){
    if (ws.readyState === ws.OPEN) {
	ws.send(msg);
    }
    else{
	alert("Sorry can't establish connection with server. Please try later");
    }
}


window.onload=function() {
}

ws.onopen = function (event) {
};

// read message from server here
ws.onmessage=function(event) {

    result = JSON.parse(event.data);
    document.getElementById('record').classList.remove("running");
    $('#myModal').modal('show');
    document.getElementById('record-notify').innerHTML = 'done !';
    display_result();
    document.getElementById("drop-result").style.visibility = "";
}

function plot_freq(){
    if (!result){
	return;
    }
    
    document.getElementById("plot").style.visibility = "";
    
    var layout = {
        paper_bgcolor: 'rgba(0.,0.,0.,0.)',
        plot_bgcolor: 'rgba(0.6,0.6,0.6,1)',
	font: {
		color: 'white'
	},
	title:'Frequency analysis',
	xaxis: {
            title: 'Frequency (Hz)',
	    color: 'white'
        },
        yaxis: {
            title: 'PSD (%)',
        },
    };

    graph_freq = document.getElementById('plot');
    Plotly.newPlot(graph_freq, result.freq, layout);
}

function plot_temp(){
    if (!result){
	return;
    }
    
    document.getElementById("plot").style.visibility = "";

    var layout = {
        paper_bgcolor: 'rgba(0.,0.,0.,0.)',
        plot_bgcolor: 'rgba(0.6,0.6,0.6,1)',
	font: {
		color: 'white'
	},
	title:'Time signal',
	xaxis: {
            title: 'Time (s)',
	    color: 'white'
        },
        yaxis: {
            title: 'signal',
        },
    };
    
    graph_time = document.getElementById('plot');
    Plotly.newPlot(graph_time, result.time, layout);

}

function display_result(){
    if (!result){
	return;
    }
    document.getElementById("plot").style.visibility = "hidden";
    document.getElementById("msg").style.visibility = "";
    document.getElementById("plot").innerHTML = "";
    document.getElementById("msg").innerHTML = result.msg;
     
}

$("#myModal").on("hidden.bs.modal", function () {
    $('#go-to-measure').click();
});


ws.onclose=function(event) {
    alert("Sorry can't establish connection with server. Please try later");
};


function stopRecording() {
    audioRecorder.stop();
    
    audioRecorder.getBuffers( gotBuffers );
    window.clearTimeout(timeoutRecord);
    document.getElementById('record-notify').innerHTML = 'getting results...';
}


function startRecording() {
    if(ws.readyState === ws.CLOSED){
	alert("Sorry,\nI can't establish connection with server.\nPlease try later.");
	return;
    }
    
    document.getElementById('record').classList.add("running");
    document.getElementById('record-notify').innerHTML = 'recording...';
    audioRecorder.clear();
    var duration = document.getElementById('duration').value * 1000;
    timeoutRecord = setTimeout(stopRecording, duration);
    audioRecorder.record();
}



function sendRecord(blob){
    var select = document.getElementById('test-type');
    msg = {
	"length": document.getElementById('length').value,
	"width": document.getElementById('width').value,
	"thickness": document.getElementById('thickness').value,
	"mass": document.getElementById('mass').value,
	"test": select.options[select.selectedIndex].value,
    }
    sendmessage("DATA"+JSON.stringify(msg));
    sendmessage(blob);
    
}
