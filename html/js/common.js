var server = "ws://127.0.0.1";
var aco_port = "9090/ws";
var opt_port = "9091/ws";
var force_https = false;

if (location.protocol != 'https:' && location.protocol != 'file:' && force_https == true){
    location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
}


$(window).on('load', function() { // makes sure the whole site is loaded 
    $('.masthead').fadeOut('slow', function() {
	$('.help-tip').addClass('stop-rotate');
    });

    $('body').delay(350).css({'overflow':'visible'});

   
})
