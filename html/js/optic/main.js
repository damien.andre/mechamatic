// Copyright 2017 Damien Andre. This file is part of Mechamatic.

// Mechamatic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// () any later version.
  
// Mechamatic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
  
// You should have received a copy of the GNU General Public License
// along with Mechamatic.  If not, see <http://www.gnu.org/licenses/>.


class Zoi {
  constructor() {
    this.x1 = 0;
    this.y1 = 0;
    this.x2 = 0;
    this.y2 = 0;
  }
}


var ws = new WebSocket(server + ":" + opt_port);
var zoi_selection = new Zoi();
var result;
var current_gif;


document.getElementById('ref-img-file').addEventListener('change', refImgFileSelect, false);
document.getElementById('process-img-file').addEventListener('change', processImgFileSelect, false);


function sendmessage(msg){
    if (ws.readyState === ws.OPEN) {
	ws.send(msg);
    }
    else{
	unwait();
	alert("Sorry can't establish connection with server. Please try later");
    }
}



window.onload=function() {
}

ws.onopen = function (event) {
};


ws.onmessage=function(event) {
    //alert("message :" + (event.data.slice(1, 100)));
    unwait();
    var jobj = JSON.parse(event.data);

    if("REF_IMG_ZOI" in jobj) {
	
	$('#zoi-modal').modal('show');
    	var img = document.getElementById('img-zoi');

	// Get real width and heigh
	var real_width, real_height;
	img.addEventListener('load', function() {
	    real_width = this.naturalWidth;
	    real_height = this.naturalHeight;
	});

	
    	img.src = "data:image/png;base64," + jobj['REF_IMG_ZOI'];
	
	$('#img-zoi').imgAreaSelect({onSelectEnd: zoiOk,
				     onSelectChange: getZoiCoordinates,
				     imageWidth : real_width,
				     imageHeight : real_height,
				     handles: true,
				     parent: '#zoi-parent' });
    }
    else if ("REF_IMG" in jobj) {
	var img = document.getElementById('img-ref');
	img.src = "data:image/png;base64," + jobj['REF_IMG'];
    }
    else if ("LAST_PROC_IMG" in jobj) {
	var img = document.getElementById('img-proc');
	img.src = "data:image/png;base64," + jobj['LAST_PROC_IMG'];
    }
    else if ("READY" in jobj) {
	$('#run-dic-btn').prop('disabled', false);
	$('#run-dic-btn').removeClass('disabled');
    }

    else if ("PROCESSING_MSG" in jobj) {
	wait(jobj['PROCESSING_MSG']);
    }

    else if ("RESULT" in jobj) {
	document.getElementById('results').style.visibility = "";
	document.getElementById('drop-result').style.visibility = "";
	result = jobj;
	plot('strain_xx');
	document.getElementById('res-msg').innerHTML = '';
	$('#result-available-dlg').modal('show');
    }
    else if ("GIF_IMG" in jobj) {
	var img = document.getElementById('gif');
	img.src = "data:image/gif;base64," + jobj['GIF_IMG'];
	if (jobj['WHAT']=='disp'){
	    document.getElementById('res-msg').innerHTML = "Note that you can adjust the displacement lines with the <em> scale</em> input (press enter to valid)";
	}
	else if (jobj['WHAT']=='grid'){
	    document.getElementById('res-msg').innerHTML = "Note that you can adjust the grid deformation with the <em> scale</em> input  (press enter to valid)";
	}
	else{
	   document.getElementById('res-msg').innerHTML = ""; 
	}
    }
    else if ("CSV_FILE" in jobj) {
	var a = document.getElementById('csv');
	a.href = "data:application/zip;base64," + jobj['CSV_FILE'];
	a.click();
    }
    
}


ws.onclose=function(event) {
    alert("Sorry can't establish connection with server. Please try later");
};



function refImgFileSelect(evt) {
    var file = evt.target.files[0];
    refImgFile(file);
}

function refImgFile(file) {
    correl_param = {
	"grid_x": document.getElementById('grid_x').value,
	"grid_y": document.getElementById('grid_y').value,
	"win_x" : document.getElementById('win_x').value,
	"win_y" : document.getElementById('win_y').value,
    };
    sendmessage("status:CORREL_DATA");
    ws.send(JSON.stringify(correl_param));

    ws.send("status:REF_IMG_NAME");
    ws.send(file.name);
    
    ws.send("status:REF_IMG");
    wait();
    ws.send(file);
}



function processImgFileSelect(evt) {
    var files = evt.target.files;
    processImgFile(files);
}


function processImgFile(files) {
    wait();
    sendmessage("status:PROCESS_IMG_INIT");
    ws.send(files.length);

    ws.send("status:PROCESS_IMG_NAME");
    for(var i= 0; i < files.length; i++){
	ws.send(files[i].name);
    }
    
    ws.send("status:PROCESS_IMG");
    for(var i= 0; i < files.length; i++){
	ws.send(files[i]);
    }
}


function zoiOk(img, selection) {
    $('#zoi-ok-btn').prop('disabled', false);
    $('#zoi-ok-btn').removeClass('disabled');
	
}


function getZoiCoordinates(img, selection) {

    if (!selection.width || !selection.height){
    return;
    }

    // With this two lines i take the proportion between the original size and
    // the resized img
    var porcX = img.naturalWidth / img.width;
    var porcY = img.naturalHeight / img.height;

    // Send the corrected coordinates to some inputs:
    // Math.round to get integer number
    // (selection.x1 * porcX) to correct the coordinate to real size
    zoi_selection.x1 = (Math.round(selection.x1 * porcX));
    zoi_selection.y1 = (Math.round(selection.y1 * porcY));
    zoi_selection.x2 = (Math.round(selection.x2 * porcX));
    zoi_selection.y2 = (Math.round(selection.y2 * porcY));
}


$('#zoi-ok-btn').on('click', function (e) {
    if ($('#zoi-ok-btn').hasClass('disabled') == false){
	$('#zoi-modal').modal('hide');
	wait();
	sendmessage("status:ZOI_IMG");

	var select = document.getElementById('marker');
	data = {
	    "zoi":zoi_selection,
	    "grid": select.options[select.selectedIndex].value,
	};
	
	ws.send(JSON.stringify(data));
    }
})



$('#zoi-all-btn').on('click', function (e) {
    $('#zoi-modal').modal('hide');
    wait();

    var img = document.getElementById('img-zoi');
    var w = img.naturalWidth;
    var h = img.naturalHeight;
    zoi_selection.x1 = 0;
    zoi_selection.y1 = 0.;
    zoi_selection.x2 = w;
    zoi_selection.y2 = h;
    
    sendmessage("status:ZOI_IMG");
    
    var select = document.getElementById('marker');
    data = {
	"zoi":zoi_selection,
	"grid": select.options[select.selectedIndex].value,
    };
    
    ws.send(JSON.stringify(data));
})



$('#run-dic-btn').on('click', function (e) {
    if ($('#run-dic-btn').hasClass('disabled') == false){
	wait();

	var select = document.getElementById('data');
	sendmessage("status:INTERPOLATION");
	interpolation = {
	    "interpolation":  select.options[select.selectedIndex].value,
	};
	ws.send(JSON.stringify(interpolation));
	ws.send("action:RUN_DIC");
    }
})


function wait(msg) {
    document.getElementById('process').style.visibility = "";
    document.getElementById('process-msg').innerHTML = "";
    if (msg){ 
	document.getElementById('process-msg').innerHTML = msg;
    }
};

function unwait() {
    document.getElementById('process').style.visibility = "hidden";
};



function gif_disp(id) {
    current_gif = id;
    
    if (!result){
	return;
    }

    if (id != "marker"){
	document.getElementById("scale_form").style.visibility = "";
    }
    
    document.getElementById('res').innerHTML = '';
    sendmessage("status:GIF");
    param = {
	"scale": document.getElementById('scale_gif').value,
	"what": id,
    };
    wait();
    ws.send(JSON.stringify(param));
    
}

function csv() {
    if (!result){
	return;
    }
    wait();
    sendmessage("action:GET_CSV");
}
    
function plot(id) {
    if (!result){
	return;
    }
    document.getElementById("scale_form").style.visibility = "hidden";
    document.getElementById('gif').src = '';
    document.getElementById('res').innerHTML = '';
    wait("processing");
    var ref_img = document.getElementById('img-ref');
    var img_width  = ref_img.naturalWidth;
    var img_height = ref_img.naturalHeight;

    

    var layout_width = document.getElementById('res').clientWidth;
    var aspect_ratio = img_width/img_height


    var res = result['RESULT'];
    var img = result['IMG_HEATMAP'];

    
    var data = [
	{
	    x: res['x'],
	    y: res['y'],
	    z: res[id],
	    type: 'contour',
	    opacity:0.8,
	},
	{
	    z: img,
	    colorscale: 'Greys',
	    type: 'heatmap',
	    showscale:false,
	    hoverinfo : 'none',
	}
    ];

    var layout =  {
	title: id,
	xaxis: {range: [0, img_width], title: 'width (pixel)'},
	yaxis: {range: [0, img_height], title: 'height (pixel)'},
	autosize: false,
        width: layout_width,
        height: layout_width/aspect_ratio,
	font: {color: 'white'},
	paper_bgcolor: 'rgba(0.,0.,0.,0.)',
        plot_bgcolor: 'rgba(0.6,0.6,0.6,1)',
	};
    
    Plotly.newPlot('res', data, layout);
    unwait();
}


$("#result-available-dlg").on("hidden.bs.modal", function () {
    $('#go-to-measure').click();
});




$('#drop-result a').click(function(){
    $('#drop-result-btn').text($(this).text());
});




$("#scale_gif").on('keyup', function (e) {
    if (e.keyCode == 13) {
        gif_disp(current_gif);
    }
});



$('#scale-ok').on('click', function (e) {
    gif_disp(current_gif);
});
