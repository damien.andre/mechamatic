#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2017 Damien Andre. This file is part of Mechamatic.

# Mechamatic is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# () any later version.
  
# Mechamatic is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
  
# You should have received a copy of the GNU General Public License
# along with Mechamatic.  If not, see <http://www.gnu.org/licenses/>.



import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
from threading import Thread
import numpy as np
import json
import os




conf = """{ 
  "freq" : [{
    "x": [_FRQ_],
    "y": [_PSD_],
    "mode": "lines",
    "line": {
       "color": "rgb(200, 30, 30)",
       "width": 4
      }
     }],
  "time" : [{
    "x": [_TIM_],
    "y": [_SIG_],
    "mode": "lines",
    "line": {
       "color": "rgb(200, 30, 30)",
       "width": 4
      }
     }],

  "msg" : "_MSG_"}
"""



def compute_E(b, t, L, m, fb):
    T=1. + 6.585*(t/L)**2.
    return 0.9465*(m*(fb**2)/b)*((L/t)**3.)*T

def compute_G(b, t, L, m, ft):
    B=((b/t)+(t/b))/(4.*(t/b)-2.52*((t/b)**2)+0.21*((t/b)**6))
    A=(0.5062-0.8776*(b/t)+0.3504*((b/t)**2)-0.0078*((b/t)**3))
    A/=(12.03*(b/t)+9.892*((b/t)**2))
    return ((4.*L*m*(ft**2))/(b*t))*(B/(1+A))





class Compute(Thread):
    def __init__(self, wav, handler):
      Thread.__init__(self)
      self.wav = wav
      self.handler = handler

    def run(self):
      signal = np.fromstring(self.wav, dtype=np.int16)
      signal = np.hstack(signal)
      signal = np.hstack(signal[::2])
      RATE = 44100.
      TIME = float(len(signal))/(RATE)
      time = np.linspace(0, TIME, num=len(signal))

      yf = np.fft.fft(signal)
      N = len(signal)
      T = 1./RATE                   # the period of data
      psd  = (T/N) * np.abs(yf)**2  # compute power spectral density 
      frq = np.fft.fftfreq(N, d=T)  # and the related frequencies
      frq = frq[:N/2] # remove negative frequencies
      psd = psd[:N/2] # and the related values of psd
      psd = (psd/np.max(psd))*100.

      
      hz_step = 10 # the step in Hz
      step    = int(hz_step*(float(N)/float(RATE)))

      # record data
      msg = conf
      msg = msg.replace("_FRQ_", ','.join(str(v) for v in frq[::step]))
      msg = msg.replace("_PSD_", ','.join(str(v) for v in psd[::step]))
      msg = msg.replace("_TIM_", ','.join(str(v) for v in time[::step]))
      msg = msg.replace("_SIG_", ','.join(str(v) for v in signal[::step]))

      # compute E or G
      rank = psd.argmax()
      maxf = frq[rank]
      text = "The natural frequency was measured at {0:.1f} Hz. ".format(maxf)

      b    = float(self.handler.data['width'])*1e-3
      t    = float(self.handler.data['thickness'])*1e-3
      L    = float(self.handler.data['length'])*1e-3
      m    = float(self.handler.data['mass'])*1e-3

      if self.handler.data['test'] == 'bending':
          E = compute_E(b, t, L, m, maxf)
          text += "The related Young's modulus E is {0:.1f} GPa".format(E*1e-9)
      else:
          G =  compute_G(b, t, L, m, maxf)
          text += "The related shear modulus G is {0:.1f} GPa".format(G*1e-9)
        
      msg = msg.replace("_MSG_", text)



      self.handler.write_message(msg)

      
    
class WSHandler(tornado.websocket.WebSocketHandler):
  def open(self):
    print('[acoustic] connection opened...')

  def on_message(self, message):
    if message.startswith("DATA") is True:
        message = message[4:]
        self.data = json.loads(message)
      
    if message.startswith("WAVE") is True:
      message = message[4:]
      comp = Compute(message, self)
      comp.run()
    
  def on_close(self):
    print('[acoustic] connection closed...')

  def check_origin(self, origin):
    return True


