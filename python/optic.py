#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2017 Damien Andre. This file is part of Mechamatic.

# Mechamatic is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# () any later version.
  
# Mechamatic is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
  
# You should have received a copy of the GNU General Public License
# along with Mechamatic.  If not, see <http://www.gnu.org/licenses/>.


import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
from threading import Thread
import numpy as np
import json
import base64
import cv2
import pydic
import os
from io import StringIO
import shutil



def Readb64(base64_string):
    sbuf = StringIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)


class Compute(Thread):
    def __init__(self):
      Thread.__init__(self)

    def run(self):
      print("compute")

      
class WSHandler(tornado.websocket.WebSocketHandler):
  
  def open(self):
    self.reset_status()
    self.grid_size = None 
    self.win_size = None 
    self.image_ref = None
    self.image_ref_name = None
    self.zoi = None
    self.process_img = None
    self.process_name = None
    self.process_img_num = None
    print('[optic] connection opened...')


  def reset_status(self):
      self.status = ""

  def on_message(self, message):

    print(type(message))
    if isinstance(message, str) == False:
        message = base64.b64encode(message).decode()
    print(type(message))
    
    print("receive message:'" + message[0:50] + "'...")

    if (message.startswith("status:")):
        self.status = message[7:]
                
    else:
        if self.status == "CORREL_DATA":
            self.reset_status()
            
            json_data = json.loads(message)
            grid_x = int(json_data['grid_x'])
            grid_y = int(json_data['grid_y'])
            win_x  = int(json_data['win_x'])
            win_y  = int(json_data['win_y'])
            self.grid_size = (grid_x,grid_y)
            self.win_size = (win_x,win_y)


        elif self.status == "REF_IMG_NAME":
            self.reset_status()
            self.image_ref_name = message

            
        elif self.status == "REF_IMG":
            self.reset_status()
            decoded_data = base64.b64decode(message)
            img_array = np.fromstring(decoded_data, np.uint8)
            self.image_ref = cv2.imdecode(img_array , cv2.IMREAD_UNCHANGED)
            msg = {}
            msg['REF_IMG_ZOI'] = base64.b64encode(cv2.imencode(".png", self.image_ref)[1]).decode("utf-8")
            self.write_message(json.dumps(msg))
            
        elif self.status == "ZOI_IMG":
            self.reset_status()
            zoi_json = json.loads(message)['zoi']
            zoi_x1 = float(zoi_json["x1"])
            zoi_y1 = float(zoi_json["y1"])
            zoi_x2 = float(zoi_json["x2"])
            zoi_y2 = float(zoi_json["y2"])
            self.zoi = [(zoi_x1, zoi_y1), (zoi_x2, zoi_y2)]

            self.grid_type = json.loads(message)['grid']
            
            # get image with markers and send it
            if (self.grid_type == 'unstructured'):
                point, point_x, point_y, img = pydic.init(self.image_ref, self.win_size, self.grid_size, "result.dic", self.zoi, unstructured_grid=(20,5))
                self.point = point
                self.point_x = point_x
                self.point_y = point_y
            else:
                point, point_x, point_y, img = pydic.init(self.image_ref, self.win_size, self.grid_size, "result.dic", self.zoi)
                self.point = point
                self.point_x = point_x
                self.point_y = point_y
                
            
            msg = {}
            msg['REF_IMG'] = base64.b64encode(cv2.imencode(".png", img)[1]).decode("utf-8")
            self.write_message(json.dumps(msg))
            self.check_is_ready()

        elif self.status == "PROCESS_IMG_INIT":
            self.reset_status()
            self.process_img      = []
            self.process_img_num  = float(message)
            self.process_img_name = []
            
        elif self.status == "PROCESS_IMG_NAME":
            self.process_img_name.append(message)
            if len(self.process_img_name) == self.process_img_num:
                self.reset_status()

            
        elif self.status == "PROCESS_IMG":
            decoded_data = base64.b64decode(message)
            img_array = np.fromstring(decoded_data, np.uint8)
            self.process_img.append(cv2.imdecode(img_array, cv2.IMREAD_UNCHANGED))
            if len(self.process_img) == self.process_img_num:
                last_img = self.process_img[-1]
                msg = {}
                msg['LAST_PROC_IMG'] = base64.b64encode(cv2.imencode(".png", last_img)[1]).decode("utf-8")
                self.reset_status()
                self.write_message(json.dumps(msg))
                self.check_is_ready()

        elif self.status == "INTERPOLATION":
            self.reset_status()
            self.interpolation = json.loads(message)['interpolation']

            # treat the unstructred case
            if (self.grid_type == 'unstructured' and self.interpolation == 'raw'):
                self.interpolation = 'linear'
            elif (self.grid_type == 'unstructured'):
                self.interpolation = 'delaunay'
                
        elif self.status == "GIF":
            self.reset_status()
            obj = json.loads(message)

            for grid in self.grid_list:
                self.send_processing_msg("building " + grid.image + " image")
                grid.draw_img(obj['what'], float(obj['scale']))
            self.send_processing_msg("building animation...")
            gif_str = self.make_gif(obj['what']).decode("utf-8")
            msg = {'GIF_IMG':gif_str, 'WHAT':obj['what']}
            self.send_processing_msg("transfering file...")
            self.write_message(json.dumps(msg))


            
    if (message.startswith("action:")):
        action = message[7:]

        if action=="RESET":
            self.reset_status()

        elif action=="RUN_DIC":
            grid_list = []

            self.res_file = pydic.run_dic(self, self.image_ref, self.process_img, self.point, self.point_x, self.point_y, self.win_size)

            self.folder = str(hex(id(self)))
            if not os.path.exists(self.folder):
                os.makedirs(self.folder)

            img      = [self.image_ref] + self.process_img
            img_name = [self.image_ref_name] + self.process_img_name

            self.grid_list = pydic.read_dic_file(self, self.res_file, img, img_name, self.folder,
                                            interpolation=self.interpolation, save_image=False, scale_disp=10,
                                            scale_grid=25)

            self.send_processing_msg("sending results...")
            im = self.image_ref
            res = {'IMG_HEATMAP':[]}

            for x in reversed(list(range(im.shape[0]))):
                k = []
                for y in range(im.shape[1]):
                    k.append(int(im[x,y]))
                res['IMG_HEATMAP'].append(k)
                
            res['RESULT'] = self.grid_list[-1].raw_result()

            jsonfile = self.folder + '/result.json'
            print('dump', jsonfile)
            with open(jsonfile, 'w') as f:
                json.dump(res, f)

            print('send', jsonfile)
            with open(jsonfile, 'rb') as f:
                data = f.read()
                self.write_message(data)
                
            
        elif action=="GET_CSV":
            zip_file = self.folder + '/result.zip'
            target   = self.folder + '/result/result'
            cmd = 'zip -j -r ' + zip_file + ' ' + target
            os.system(cmd)
            encoded_string = ""
            with open(zip_file, "rb") as _file:
                encoded_string = base64.b64encode(_file.read()).decode("utf-8")
            res = {'CSV_FILE':encoded_string}
            self.write_message(json.dumps(res))

  
  def check_is_ready(self):
    # check if ready
    if (self.grid_size is not None and
        self.win_size is not None and
        self.image_ref is not None and
        self.image_ref_name is not None and
        self.zoi is not None and
        self.process_img is not None and
        self.process_img_num is not None and
        self.process_img_name is not None):
        if (len(self.process_img) > 0 and
            len(self.process_img) == self.process_img_num and
            len(self.process_img_name) == self.process_img_num):
            msg = {}
            msg['READY'] = True
            self.write_message(json.dumps(msg))    

  def send_processing_msg(self, message):
      msg = {}
      msg['PROCESSING_MSG'] = message
      self.write_message(json.dumps(msg))
            
  def make_gif(self, name):
      gif_file = self.folder + '/result/' + name + '.gif'
      cmd = 'convert -delay 100 -loop 0 ' + self.folder + '/result/' + name + '/*.png ' + gif_file
      os.system(cmd)

      # while (os.path.getsize(gif_file) > 2000000):
      #     os.system('convert ' + gif_file + ' -resize 50% ' + gif_file)

      encoded_string = ""
      with open(gif_file, "rb") as image_file:
          encoded_string = base64.b64encode(image_file.read())

      return encoded_string
      
    
  def on_close(self):
      print('[optic] connection closed...')
      if hasattr(self, 'folder'):
          if os.path.exists(self.folder):
              shutil.rmtree(self.folder)

  def check_origin(self, origin):
      return True

