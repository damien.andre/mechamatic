from os import listdir
from os.path import isfile, join
import threading
import subprocess

#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2017 Damien Andre. This file is part of Mechamatic.

# Mechamatic is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# () any later version.
  
# Mechamatic is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
  
# You should have received a copy of the GNU General Public License
# along with Mechamatic.  If not, see <http://www.gnu.org/licenses/>.



# This script file is an util file for running mechamatic with always up-to-date cerificate SSL
# certificate given by let's encrypt. If a server is stopped, it automatically re-run it with
# the most up-to-date certificate.

# Please use this script if you have experienced SSL expiration problems of your certificate.


SSL_PATH = "/etc/letsencrypt/archive/yakuru.fr/" # The path where the SSL cerificates are located


def updated_cert_files():
    cert_files      = [f for f in listdir(SSL_PATH) if isfile(join(SSL_PATH, f)) and 'cert' in f]
    cert_num        = [int(list(filter(str.isdigit, f))) for f in cert_files]
    most_recent_num = max(cert_num)
    cert_file = SSL_PATH + 'cert'    + str(most_recent_num) + '.pem'
    pkey_file = SSL_PATH + 'privkey' + str(most_recent_num) + '.pem'
    return (cert_file, pkey_file)



def popenAndCall(onExit, popenArgs):
    """
    Runs the given args in a subprocess.Popen, and then calls the function
    onExit when the subprocess completes.
    onExit is a callable object, and popenArgs is a list/tuple of args that 
    would give to subprocess.Popen.
    """
    def runInThread(onExit, popenArgs):
        print('[RUN] ', popenArgs)
        proc = subprocess.Popen(popenArgs)
        proc.wait()
        onExit()
        return
    thread = threading.Thread(target=runInThread, args=(onExit, popenArgs))
    thread.start()
    # returns immediately after the thread starts
    return thread



def run_acoustic():
    cert_file, pkey_file = updated_cert_files()
    popenAndCall(run_acoustic, ['python', './run-server.py', '--cert', cert_file, '--key', pkey_file, '--acoustic'])


def run_optic():
    cert_file, pkey_file = updated_cert_files()
    popenAndCall(run_optic, ['python', './run-server.py', '--cert', cert_file, '--key', pkey_file, '--optic'])


    
run_acoustic()
run_optic()
    
