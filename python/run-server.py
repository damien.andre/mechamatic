#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2017 Damien Andre. This file is part of Mechamatic.

# Mechamatic is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# () any later version.
  
# Mechamatic is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
  
# You should have received a copy of the GNU General Public License
# along with Mechamatic.  If not, see <http://www.gnu.org/licenses/>.



import logging
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
import os
import argparse
import acoustic
import optic
import sys
import traceback

# default port 
PORT = 9090


class MainHandler(tornado.web.RequestHandler):
  def get(self):
    loader = tornado.template.Loader(".")
    self.write(loader.load("index.html").generate())


app_acoustic = tornado.web.Application([
  (r'/ws', acoustic.WSHandler),
  (r'/', MainHandler),
  (r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
])


app_optic = tornado.web.Application([
  (r'/ws', optic.WSHandler),
  (r'/', MainHandler),
  (r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
])


# it stops the server if an SSL error is detected
class SSL_Expired_Filter(logging.Filter):
  def filter(self, record):
    if 'error on read' in record.getMessage():
      if ' sslv3 alert certificate expired' in traceback.format_exc():
        print('[ERROR] Your ssl certificate is expired')
        print('[ ... ] Please re-run server with an up-to-date certificate')
        print('[ ... ] Exiting...')        
        sys.exit(0)
    return True



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--cert", help="path of ssl certificate", type=str)
    parser.add_argument("--key" , help="path of ssl keys", type=str)
    parser.add_argument("--port", help="port number", type=int)
    parser.add_argument("--acoustic", help="run acoustic server", action="store_true")
    parser.add_argument("--optic"   , help="run optic server", action="store_true")

    args = parser.parse_args()

    run_acoustic = True
    run_optic    = True


    
    if args.acoustic:
        run_acoustic = True
        run_optic    = False

    if args.optic:
        run_acoustic = False
        run_optic    = True

        
    port = PORT
    if args.port :
        port = args.port
        
    ssl = None
    if  args.cert and args.key:
        ssl = {"certfile": args.cert, "keyfile": args.key}
        

    if run_acoustic:
        print("run acoustic server on port", port)
        app_acoustic.listen(port, ssl_options=ssl)

    if run_optic:
        print("run optic server on port", port + 1)
        app_optic.listen(port+1, ssl_options=ssl)


    logging.getLogger('tornado.general').addFilter(SSL_Expired_Filter())
    tornado.ioloop.IOLoop.instance().start()

